#include <stdio.h>
int main() {
    int num, reversednum = 0, remain = 0;
    printf("Enter a Number \n");
    scanf("%d", &num);
    while (num != 0) {
        remain = num % 10;
        reversednum = reversednum * 10 + remain;
        num /= 10;
    }
    printf("Reversed number = %d", reversednum);
    return 0;
}
